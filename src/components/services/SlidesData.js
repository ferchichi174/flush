import { shineLogo } from '../AllImages'

const SlidesData = [
    {
        img: shineLogo,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat venenatis congue amet, in id scelerisque egestas. Tempus cursus cras cras aliquam consectetur mi vel nec. Volutpat nulla id nulla et dictum at faucibus. Sed consequat libero, feugiat diam, non proin velit vitae molestie.',
        title: "Director of innovation"
    },
    {
        img: shineLogo,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat venenatis congue amet, in id scelerisque egestas. Tempus cursus cras cras aliquam consectetur mi vel nec. Volutpat nulla id nulla et dictum at faucibus. Sed consequat libero, feugiat diam, non proin velit vitae molestie.',
        title: "Director of innovation"
    },
    {
        img: shineLogo,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat venenatis congue amet, in id scelerisque egestas. Tempus cursus cras cras aliquam consectetur mi vel nec. Volutpat nulla id nulla et dictum at faucibus. Sed consequat libero, feugiat diam, non proin velit vitae molestie.',
        title: "Director of innovation"
    },
    {
        img: shineLogo,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat venenatis congue amet, in id scelerisque egestas. Tempus cursus cras cras aliquam consectetur mi vel nec. Volutpat nulla id nulla et dictum at faucibus. Sed consequat libero, feugiat diam, non proin velit vitae molestie.',
        title: "Director of innovation"
    },
    {
        img: shineLogo,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat venenatis congue amet, in id scelerisque egestas. Tempus cursus cras cras aliquam consectetur mi vel nec. Volutpat nulla id nulla et dictum at faucibus. Sed consequat libero, feugiat diam, non proin velit vitae molestie.',
        title: "Director of innovation"
    },
    {
        img: shineLogo,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat venenatis congue amet, in id scelerisque egestas. Tempus cursus cras cras aliquam consectetur mi vel nec. Volutpat nulla id nulla et dictum at faucibus. Sed consequat libero, feugiat diam, non proin velit vitae molestie.',
        title: "Director of innovation"
    },
    {
        img: shineLogo,
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat venenatis congue amet, in id scelerisque egestas. Tempus cursus cras cras aliquam consectetur mi vel nec. Volutpat nulla id nulla et dictum at faucibus. Sed consequat libero, feugiat diam, non proin velit vitae molestie.',
        title: "Director of innovation"
    },
]

export default SlidesData