import React from "react"
import styled from 'styled-components'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import 'react-lazy-load-image-component/src/effects/blur.css'

const ServicesNavStyled = styled.nav`

    width: 100%;
    min-height: 95px;
    background: rgb(26, 117, 188, 0.05);
    border-radius: 5px;
    margin-bottom: 60px;
    padding: 10px 50px;
    &,ul{
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        justify-content: space-between;
    }
    ul{
        list-style: none;
        width:100%;
        margin:0;
    }
    li{
        position: relative;
        a {
            position: relative;
            color: var(--theme-clr);
            font-weight: 600;
            font-size: 14px;
            line-height: 17px;
            white-space: nowrap;
            margin: 15px 10px;
            background-color: transparent;
            border: 0;
        }
        &.active {
            &::after {
                content: "";
                position: absolute;
                background-color: var(--text-clr);
                border-radius: 50%;
                width: 6px;
                height: 6px;
                bottom: -10px;
                left: 50%;
                transform: translateX(-50%);
            }
        }
    }
    @media (max-width:992px) {
        justify-content: center;
    }
    @media (max-width:768px) {
        padding: 10px 20px;
        button {
            text-align: center;
            margin: 15px 0;
            flex-basis: 33%;
        }
    }
    @media (max-width:576px) {
        button {
            font-size: 13px;
            flex-basis: 50%;
        }
    }
`;

const ServicesText = styled.h1`
    font-weight: 600;
    font-size: 24px;
    line-height: 29px;
    margin: ${props => props.margin};
    text-align: center;
    text-shadow: 0px 100px 80px rgba(0, 0, 0, 0.15),
    0px 64.8148px 46.8519px rgba(0, 0, 0, 0.113889),
    0px 38.5185px 25.4815px rgba(0, 0, 0, 0.0911111),
    0px 20px 13px rgba(0, 0, 0, 0.075),
    0px 8.14815px 6.51852px rgba(0, 0, 0, 0.0588889),
    0px 1.85185px 3.14815px rgba(0, 0, 0, 0.0361111);
    span {
        opacity: 0.1;
        margin-right: 10px;
    }
    @media (max-width: 768px) {
        font-size: 22px;
    }
    @media (max-width: 576px) {
        font-size: 18px;
        line-height: 24px;
    }
`;

const ServicesImage = ({
    img,
    classN,
}) => {
    return (
        <ServicesImageStyled className={classN}>
            <LazyLoadImage
                alt="image"
                effect="blur"
                src={img}
                className="d-block"
            />
        </ServicesImageStyled>
    )
}

const ServicesImageStyled = styled.div`
    width: 100%;
    &.h-175 {
        span {
            img {
                min-height: 175px;
            }
        }
    }
    span {
        width: 100%;
        height: auto;
    }
    img {
        width: 100%;
        height: auto;
        object-fit: cover;
        border-radius: 5px;
        box-shadow: 4.03317px 99.9186px 80px rgba(0, 0, 0, 0.0168519),
        2.61409px 64.7621px 46.8519px rgba(0, 0, 0, 0.0274815),
        1.55352px 38.4872px 25.4815px rgba(0, 0, 0, 0.035),
        0.806635px 19.9837px 13px rgba(0, 0, 0, 0.0425185),
        0.328629px 8.14152px 6.51852px rgba(0, 0, 0, 0.0531481),
        0.0746884px 1.85035px 3.14815px rgba(0, 0, 0, 0.07);
    }
`;

const ServiceDetail = ({
    classN,
    Icon,
    title,
    descp,
}) => {
    return (
        <ServiceDetailStyled className={classN}>
            <div className="icon center">
                <Icon />
            </div>
            <div className="flex-1">
                <h1>{title}</h1>
                <p>{descp}</p>
            </div>
        </ServiceDetailStyled>
    )
}

const ServiceDetailStyled = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 70px;
    padding-right: 50px;
    .icon {
        width: 96px;
        height: 96px;
        border-radius: 50%;
        background: #1A75BC;
        box-shadow: inset 0px 0px 30px rgba(0, 0, 0, 0.2);
        margin-right: 50px;
        svg {
            max-width: 32px;
            max-height: 32px;
        }
    } 
    h1 {
        font-weight: 700;
        font-size: 14px;
        line-height: 17px;
        color: var(--text-clr);
        margin-bottom: 5px;
    }
    p {
        font-weight: 400;
        font-size: 14px;
        line-height: 17px;
        color: var(--text-clr);
        margin: 0;
    }
    &.flex-column-start {
        flex-direction: column;
        &.text-center {
            .icon {
                margin-left: auto;
                margin-right: auto;
            }
        }
        .icon {
            margin-bottom: 20px;
            margin-right: auto;
        }
    }
    @media (max-width:992px) {
        padding-right: 30px;
    }
    @media (max-width:768px) {
        padding-right: 0;
        &.flex-column-start {
            text-align: left !important;
            flex-direction: row;
            &.text-center {
                .icon {
                    margin-left: inherit;
                    margin-right: 30px;
                }
            }
            .icon {
                margin-bottom: 0;
                margin-right: 30px;
            }
        }
        .icon {
            margin-right: 30px;
        }
    }
`;

const ClientsSlider = styled.div`
    .swiper {
        padding-bottom: 200px;
        .swiper-pagination {
            bottom: 120px !important;
            .swiper-pagination-bullet {
                background: var(--text-clr);
                opacity: 0.05;
                width: 13px;
                height: 13px;
            }
            .swiper-pagination-bullet-active {
                opacity: 1;
                height: 17px;
                transform: translateY(4px);
            }
        }
        .swiper-slide {
            opacity: 0.2;
            transform: scale(0.8);
            .slidee {
                transform: translateY(50px);
            }
        }
        .swiper-slide-active {
            opacity: 1;
            transform: scale(1);
            .slidee {
                transform: translateY(0);
            }
        }
    }
`;

const ClientSlides = ({ data }) => {
    return (
        <ClientSlidesStyled className="slidee">
            <img src={data.img} alt="logo" />
            <p>{data.text}</p>
            <h6>{data.title}</h6>
        </ClientSlidesStyled>
    )
}

const ClientSlidesStyled = styled.div`
    min-height: 370px;
    border: 1px solid #BDBDBD;
    box-sizing: border-box;
    border-radius: 5px;
    text-align: center;
    padding: 30px;
    img {
        margin-bottom: 32px;
        max-width: 150px;
        max-height: 100px;
    }
    p {
        font-weight: 400;
        font-size: 14px;
        line-height: 17px;
        margin-bottom: 10px;
        min-height: 150px;
    }
    h6 {
        font-weight: 700;
        font-size: 14px;
        line-height: 17px;
        color: #BDBDBD;
        margin: 0;
    } 
    @media (max-width:576px) {
        padding: 30px 15px;
    }
`;

export {
    ServicesNavStyled,
    ServicesText,
    ServicesImage,
    ServiceDetail,
    ClientSlides,
    ClientsSlider,
}