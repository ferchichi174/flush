import styled from "styled-components"

const TestimonialBox = styled.div`
    box-shadow: 4.03317px 99.9186px 80px rgba(0, 0, 0, 0.0168519), 2.61409px 64.7621px 46.8519px rgba(0, 0, 0, 0.0274815), 
    1.55352px 38.4872px 25.4815px rgba(0, 0, 0, 0.035), 0.806635px 19.9837px 13px rgba(0, 0, 0, 0.0425185), 
    0.328629px 8.14152px 6.51852px rgba(0, 0, 0, 0.0531481), 0.0746884px 1.85035px 3.14815px rgba(0, 0, 0, 0.07);
    border-radius: 5px;
    padding:30px;
`;

const EstimateNavigate = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 30px 0;
    border-top: 2px solid rgb(26, 117, 188, 0.2);
    button {
        white-space: nowrap;
    }
    @media (max-width:450px) {
        flex-direction: column;
        padding: 10px 0;
        button {
            margin: 10px 0;
        }
    }
`;

export {
    TestimonialBox,
    EstimateNavigate,
}