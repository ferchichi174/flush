const Responsibilities = [
  {
    text: "Design, enable, and deliver scalable and automated strategy – Infrastructure-As-Code.",
  },
  {
    text: "Build DevOps roadmap for the organization and drive teams towards the same.",
  },
  {
    text: "Enable the “DevOps” mindset into the organization by working collaboratively with the Dev and operations team.",
  },
  {
    text: "Ensure appropriate CI/CD pipelines are built and executed with automation and utilization of appropriate DevOps tools – on both Cloud and on premise infrastructure.",
  },
  {
    text: "Enable the “DevOps” mindset into the organization by working collaboratively with the Dev and operations team.",
  },
  {
    text: "esign, develop, and support all AWS activities for our environments.",
  },
  {
    text: "Ability to install and configure software, perform debugging writing scripts and running diagnostics.",
  },
  {
    text: "Advanced knowledge of best practices for data encryption and cybersecurity.",
  },
  {
    text: "Expert level in automated builds and deployments across multiple tool sets.",
  },
  {
    text: "Dedicated Servers maintenance and local Data center maintenance inducing GPUs, SAN server and storage boxes.",
  },
  {
    text: "Troubleshooting any issue and to keep sure that system is up and running and development teams are not facing any delay.",
  },
  { text: "Configuration of web servers, security policies implementations." },
  {
    text: "Scalability of the architecture, and resolving limitations in scalability.",
  },
];
const Skills = [
  {
    text: "Strong programming skills to write algorithms and logic in tools in bash scripting such as some backup scripts.",
  },
  {
    text: "In depth understanding of GCP cloud services, console and cloud API, dedicated servers, bare metal hardware maintenance.",
  },
  {
    text: "Automation of deployment, building deployment pipelines with appropriate testing procedures.",
  },
  {
    text: "Deployment of services on GPU machines, SAN Server, Storage Box and managing/monitoring resources. Experience with building CI/CD pipelines using tools like Jenkins.",
  },
  {
    text: "Web Server configuration, tuning, and optimization, such as Nginx, apache.",
  },
  {
    text: "Experience with container orchestration using docker. Kubernetes knowledge would be plus.",
  },
  {
    text: "Good understanding of applications deployment, like PHP, JS python, node base applications.Itch to optimize things proactively.",
  },
  {
    text: "Good understanding of computational time/space complexity and data structures/algorithms to use to optimize the system.",
  },
  {
    text: "Proficient in tools for logging, alerting and monitoring cloud infrastructure, services and performance. Such as grafana, ELK stack.",
  },
  {
    text: "Experience with Continuous Integration/ Deployment mechanism using Jenkins, Nexus, Docker Registry, Gitlab, Ansible/Terraform.",
  },
  {
    text: "Good Knowledge of SaaS, Cloud Infrastructure, and other enterprise-related technologies (AWS).",
  },
  { text: "Experience with AWS and Google Cloud." },
  { text: "Experience with Container orchestration (Kubernetes)." },
  { text: "Strong scripting skills (Shell scripting, Git and Git workflows)." },
  { text: "Strong knowledge of Unix-based systems." },
  { text: "Experience with version control (Git)." },
  { text: "Deployment and configuration tools (Ansible, Chef, Puppet, etc.)." },
  {
    text: "Experience in developing Continuous Integration/ Continuous Delivery pipelines (CI/ CD).",
  },
  {
    text: "Working knowledge of networking technologies such as switching, routing, firewalls, and load balancing for high-performance highly-available web applications.",
  },
  {
    text: "Extensive knowledge of custom firewalls, (IP Tables) and Networking or network related issues.",
  },
  { text: "Extensive knowledge of the security policies and implementations." },
  {
    text: "Database deployments, backups, and PITRs and optimization knowledge, PGSQL, MongoDB and MySQL experience is a must.",
  },
  { text: "Good learning and R&D skills." },
];
export { Responsibilities, Skills };
