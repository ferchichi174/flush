import {
    mblImg,
} from '../AllImages'

const HomeSlideData = [
    {
        heading: "Transforming the UX Design of a rising Fintech star in Mexico",
        descp: "Whether you're a Fortune 500 or a startup in stealth mode - we give our clients the creative, technical and business talent they need to succeed.",
        img: mblImg
    },
    {
        heading: "Transforming the UX Design of a rising Fintech star in Mexico",
        descp: "Whether you're a Fortune 500 or a startup in stealth mode - we give our clients the creative, technical and business talent they need to succeed.",
        img: mblImg
    },
    {
        heading: "Transforming the UX Design of a rising Fintech star in Mexico",
        descp: "Whether you're a Fortune 500 or a startup in stealth mode - we give our clients the creative, technical and business talent they need to succeed.",
        img: mblImg
    },
    {
        heading: "Transforming the UX Design of a rising Fintech star in Mexico",
        descp: "Whether you're a Fortune 500 or a startup in stealth mode - we give our clients the creative, technical and business talent they need to succeed.",
        img: mblImg
    },
    {
        heading: "Transforming the UX Design of a rising Fintech star in Mexico",
        descp: "Whether you're a Fortune 500 or a startup in stealth mode - we give our clients the creative, technical and business talent they need to succeed.",
        img: mblImg
    },
]

export {
    HomeSlideData,
}