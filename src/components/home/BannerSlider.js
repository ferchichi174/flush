import React from 'react'
import { Animated } from "react-animated-css"
import { Swiper, SwiperSlide } from 'swiper/react'
import { Autoplay } from 'swiper'
import { EffectFade } from 'swiper'
import 'swiper/css/effect-fade'
import 'swiper/css'
import "swiper/css/bundle"
import 'swiper/css/autoplay'
import {
    BannerSliderStyled,
    BannerSlide
} from '../home/Components'
import {
    banner1,
    banner2,
    sloganWheel,
    HomeVector5,
} from '../AllImages'

const BannerSlider = () => {
    return (
        <>
            <BannerSliderStyled>
                <img src={sloganWheel} alt="wheel" className="wheel" />
                <Swiper
                    spaceBetween={0}
                    slidesPerView={1}
                    autoplay
                    effect="fade"
                    modules={[EffectFade, Autoplay]}
                >
                    <SwiperSlide>
                        <BannerSlide style={{ backgroundImage: `url(${banner1})` }} />
                    </SwiperSlide>
                    <SwiperSlide>
                        <BannerSlide style={{ backgroundImage: `url(${banner2})` }} />
                    </SwiperSlide>
                </Swiper>
                <div className="bg-layer"></div>
                <Animated animationIn="bounceInDown" animationOut="fadeOut" isVisible={true}>
                    <HomeVector5 className="vector" />
                </Animated>
            </BannerSliderStyled>
        </>
    )
}

export default BannerSlider