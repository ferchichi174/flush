import styled from 'styled-components'
import 'react-lazy-load-image-component/src/effects/blur.css'
import { Row, Col } from 'react-bootstrap'

const BannerText = styled.h1`
  font-weight: 600;
  font-size: 40px;
  line-height: 49px;
  text-align: center;
  margin: 0 auto 20px auto;
  max-width: 768px;
  @media (max-width: 768px) {
    font-size: 30px; 
  }
  @media (max-width: 576px) {
    font-size: 24px; 
    line-height: 40px;
  }
`;

const BannerSliderStyled = styled.div`
    position: relative;
    max-width: 471px;
    width: 100%;
    margin-bottom: 125px;
    margin-left: auto;
    margin-right: auto;
    .wheel {
        position: absolute;
        bottom: 150px;
        z-index: 50;
        left: -71px;
        border: 8px solid #f7f7f7;
        border-radius: 50%;
        background-color: #f7f7f7;
        animation: rotate 12s linear infinite;
        @keyframes rotate {
            from {
                transform: rotate(0);
            }
            to {
                transform: rotate(360deg);
            }
        }
    }
    .bg-layer {
        position: absolute;
        width: 386px;
        height: 610px;
        left: 55%;
        top: 76px;
        background: linear-gradient(180deg, #1A75BC 23.26%, rgba(26, 117, 188, 0) 111.64%);
        opacity: 0.3;
        border-radius: 235px;
        transform: matrix(1, 0, 0, -1, 0, 0);
    }
    .vector {
        position: absolute;
        bottom: -300px;
        right: -50px;
        z-index: -1;
    }
    @media (max-width:992px) {
        margin-top: 60px;
        margin-bottom: 80px;
        .bg-layer {
            left: 45%;
        }
    }
    @media (max-width:768px) {
        .bg-layer {
            left: 50%;
            transform: translateX(-50%);
            bottom: -60px;
            top: auto;
        }
        .wheel {
            left: -60px;
        }
    }
    @media (max-width:600px) {
        .wheel, .vector {
            display: none;
        }
    }
    @media (max-width:576px) {
        .bg-layer {
            width: 90%;
        }
    }
`;

const BannerSlide = styled.div`
    width: 100%;
    height: 742px;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    position: relative;
    border-radius: 235px;
    &::after {
        content: "";
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        background: #1A75BC;
        opacity: 0.1;
        border-radius: 235px;
    }
    @media (max-width:576px) {
        height: 710px;
    }
`;

const HomeCard = ({
    classN,
    number,
    title,
    img,
}) => {
    return (
        <HomeCardStyled className={classN}>
            <img src={img} alt={title} />
            <h1>{number}</h1>
            <h2>{title}</h2>
        </HomeCardStyled>
    )
}

const HomeCardStyled = styled.div`
    position: relative;
    width: 100%;
    height: 243px;
    border-radius: 5px;
    margin-bottom: 50px;
    padding: 30px 15px;
    display: flex;
    flex-direction: column;
    justify-content: end;
    align-items: center;
    cursor: pointer;
    overflow: hidden;
    &.translate {
        transform: translateY(50px);
    }
    &:hover {
        img {
            transform: scale(1.1);
        }
    }
    img {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        transition: transform 0.3s ease-in-out;
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 5px;
    }
    &::after {
        content: "";
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        background: #2E2E2E;
        opacity: 0.6;
        z-index: 0;
        border-radius: 5px;
    }
    h1, h2 {
        position: relative;
        z-index: 10;
        font-weight: 700;
        color: #F7F7F7;
        text-align: center;
        padding: 0 10px;
    }
    h1 {
        font-size: 24px;
        line-height: 29px;
        margin-bottom: 26px;
        &::after {
            content: "";
            position: absolute;
            bottom: -6px;
            left: 50%;
            transform: translateX(-50%);
            width: 24px;
            height: 5px;
            background-color: var(--theme-clr);
        }
    }
    h2 {
        font-size: 14px;
        line-height: 17px;
        margin-bottom: 0;
    }
    @media (max-width:768px) {
        height: 280px;
        margin-bottom: 30px;
        &.translate {
            transform: translateY(0);
        }
    }
    @media (max-width:400px) {
        height: 250px;
    }
`;

const HomeSlides = ({ data }) => {
    return (
        <HomeSlidesStyled>
            <Row className="align-items-center">
                <Col lg={6}>
                    <h1>{data.heading}</h1>
                    <p>{data.descp}</p>
                </Col>
                <Col lg={6} className="text-center">
                    <img src={data.img} alt="Image" className="img img-fluid" />
                </Col>
            </Row>
        </HomeSlidesStyled>
    )
}

const HomeSlidesStyled = styled.div`
    max-width: 1044px;
    margin: 50px auto 125px auto;
    padding-top: 30px;
    color: var(--text-clr);
    h1 {
        font-weight: 700;
        font-size: 16px;
        line-height: 20px;
        margin-bottom: 10px;
    }
    p {
        font-weight: 400;
        font-size: 14px;
        line-height: 17px;
    }
    .swiper-pagination {
        top: 0;
    }
    @media (max-width:992px) {
        text-align: center;
        .img {
            margin: 50px auto 0 auto;
        }
    }
    @media (max-width:576px) {
        p {
            font-size: 13px;
        }
    }
`;

export {
    BannerText,
    BannerSliderStyled,
    BannerSlide,
    HomeCard,
    HomeSlides,
}