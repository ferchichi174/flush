import React from "react";
import ReactApexChart from "react-apexcharts";
import { Box } from "../Style";
class ApexChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        colors: ["#1A75BC"],
        dataLabels: {
          enabled: false,
        },
        chart: {
          toolbar: {
            show: false,
          },
        },
        xaxis: {
          categories: [2010, 2012, 2013, 2014, 2015, 2017, 2019, 2020],
        },
        plotOptions: {
          bar: {
            columnWidth: "45px",
            colors: {
              backgroundBarColors: ["#1A75BC"],
              backgroundBarOpacity: 0.1,
              backgroundBarRadius: 5,
            },
          },
        },
        grid: {
          show: false,
        },
      },
      series: [
        {
          name: "series-1",
          data: [5, 10, 15, 20, 29, 40, 50, 65],
        },
      ],
    };
  }

  render() {
    return (
      <Box className="w-100 pe-5 mt-4">
        <ReactApexChart
          options={this.state.options}
          series={this.state.series}
          type="bar"
          height={300}
          width="100%"
        />
      </Box>
    );
  }
}
export default ApexChart;
