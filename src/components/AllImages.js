import purchase from '../assets/images/purchase pop-up.png'
import user1 from '../assets/images/user-1.png'
import user2 from '../assets/images/user-2.png'
import user3 from '../assets/images/user-3.png'
import user4 from '../assets/images/user-4.png'
import user5 from '../assets/images/user-5.png'
import personImg from '../assets/images/personImg.png'
import view from '../assets/images/view.png'
import like from '../assets/images/like.png'
import unlimited from '../assets/images/unlimited.png'
import searchIcon from '../assets/images/svg/search-icon.svg'
import leftArrow from '../assets/images/svg/left-arrow.svg'
import rightArrow from '../assets/images/svg/right-arrow.svg'
import attachPin from '../assets/images/svg/attach-pin.svg'
import CameraPana from '../assets/images/svg/camera-pana.svg'
import PushNotifications from '../assets/images/svg/push-notification.svg'
import LocationSearch from '../assets/images/svg/location-search.svg'
import FemalePicture from '../assets/images/female.jpg'
import MalePicture from '../assets/images/male.jpg'
import BlankPicture from '../assets/images/blank.png'
import HebrewLangaugeFlag from '../assets/images/hebrew.png'
import EnglishLangaugeFlag from '../assets/images/english.png'
import PremiumAvatar from '../assets/images/premium.png'
import LoginBg from '../assets/images/login-bg.jpg'
import LoginBgMobile from '../assets/images/jojo.jpg'
import Avatar from '../assets/images/avatar.jpg'
// import Logo from '../assets/images/logo.png'
import Logo from '../assets/images/jojo.png'
import CalendarIcon from '../assets/images/calendar-icon.png'
import userIcon from '../assets/images/user.png'
import ArrowDownIcon from '../assets/images/angle-down.png'
import ArrowLeft from '../assets/images/arrow-left.png'
import { ReactComponent as DashboardIcon } from '../assets/images/svg/dashboard.svg'
import { ReactComponent as ReportIcon } from '../assets/images/svg/application.svg'
import { ReactComponent as RequestIcon } from '../assets/images/svg/project-request.svg'
import { ReactComponent as CategoryIcon } from '../assets/images/svg/category.svg'
import { ReactComponent as WebSetting } from '../assets/images/svg/web-setting.svg'
import { ReactComponent as JobManagmentIcon } from '../assets/images/svg/job_managment.svg'
import { ReactComponent as HomeIcon } from '../assets/images/svg/home.svg'
import { ReactComponent as StarIcon } from '../assets/images/svg/star.svg'
import { ReactComponent as MessageIcon } from '../assets/images/svg/message.svg'
import { ReactComponent as UserIcon } from '../assets/images/svg/user.svg'
import { ReactComponent as FilterIcon } from '../assets/images/svg/filter.svg'
import Trash from '../assets/images/trash.png'
import locationIcon from '../assets/images/location-icon.png'
import man from '../assets/images/man.png'
import globe from '../assets/images/globe.png'
import balance from '../assets/images/balance.png'
import eye from '../assets/images/eye.png'
import education from '../assets/images/education.png'
import body from '../assets/images/body.png'
import cheers from '../assets/images/cheers.png'
import Homepage from '../assets/images/homepage.png'
import abstract from '../assets/images/abstract-shape.png'
import { ReactComponent as DiscoverIcon } from '../assets/images/svg/discover.svg'


export {
    man,
    DiscoverIcon,
    globe,
    balance,
    education,
    body,
    eye,
    abstract,
    Homepage,
    cheers,
    purchase,
    searchIcon,
    FilterIcon,
    locationIcon,
    user1,
    user2,
    user3,
    user4,
    user5,
    HomeIcon,
    StarIcon,
    MessageIcon,
    LocationSearch,
    UserIcon,
    CameraPana,
    PushNotifications,
    leftArrow,
    attachPin,
    rightArrow,
    ArrowDownIcon,
    personImg,
    unlimited,
    like,
    view,
    MalePicture,
    FemalePicture,
    BlankPicture,
    HebrewLangaugeFlag,
    EnglishLangaugeFlag,
    PremiumAvatar,
    LoginBg,
    LoginBgMobile,
    DashboardIcon,
    JobManagmentIcon,
    WebSetting,
    CategoryIcon,
    RequestIcon,
    ReportIcon,
    Avatar,
    ArrowLeft,
    Logo,
    CalendarIcon,
    userIcon,
    Trash
}