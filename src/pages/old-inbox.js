import React, { useEffect, useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import {
  searchIcon,
  user1,
  user2,
  user3,
  user4,
  user5,
  attachPin,
  rightArrow,
  Trash,
} from "../components/AllImages";
import { Image } from "../components/Components";
import { Box, Heading } from "../components/Style";
import { UpgradePremiumModel } from "../components/Ui/Popups";
import { deleteUserChat, galleryDeleteApi, GetAllConnections, getGallery, seenMessages, uploadGalleryAPi, keywordsGet } from "../constant/api/Api";
import { ASSET_URL, IMAGE_URL, SETMESSAGES } from "../constant/Keys";
import { userAvatar } from "../helpers/helper";
import { sendMessagesAction } from "../redux/actions/ClientActions";
import { Link } from "react-router-dom";
import InputEmoji from "react-input-emoji";
import { toast } from "react-toastify";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import Dropdown from "react-bootstrap/Dropdown";
import { Button, Col, Modal, Row } from "react-bootstrap";
import Swal from "sweetalert2";
import config from '../config.json';

let socket = null;

const Inbox = ({lang}) => {  
  const { messages } = useSelector((state) => state.clients);
  const { user, admin_user } = useSelector((state) => state.auth);

  const [activeuser, setActiveUser] = useState(null);
  const [sendData, setSendData] = useState({  message: "" });
  const [media,setMedia] = useState([]);
  const [galleryMedia,setGalleryMedia] = useState([]);
  const [selectedMediaGallery,setSelectedMediaGallery] = useState([]);
  const [modalView, setModalView] = useState(false);
  const [modalShowGallery, setModalShowGallery] = React.useState(false)
  const [filterdUsers, setFilterdUsers] = useState(false);
  const [searchedValue, setSearchedValue] = useState("");
  const url = new URL(window.location.href);
  const { langauge } = useSelector((state)=>state.clients);
  const [del_loader,setDelLoader] = useState(false);
  const chat___id = url.searchParams.get("u");
  const dispatch = useDispatch();
  const bottomRef = useRef();
  const [selectGallery,setSelectedGallery] = useState([]);
  const [words, setWords] = useState([]);
  const user__conection = admin_user && new URL(window.location.href).pathname.includes('chat/self') ? admin_user : user;

  const seenMessgaeApi = async (list) => {
    if(list?.unread_messages>0){
      try{
        await seenMessages({connection_id:list.id});
      }
      catch(error){

      }
    }
  }


  const renderTooltip = (props) => (
    <Tooltip {...props}>
       {`Delete all ${   
        activeuser?.connected_to_user?.id === user__conection.id?
        activeuser?.connected_by_user?.name:
        activeuser?.connected_to_user?.name} chat`
      }
    </Tooltip>
  );

  useEffect(() => {
    const token = localStorage.datingAppauthToken;
    socket = new WebSocket(`${config.websocket}?token=${token}`);
    socket.on = (connection) => {
      console.log("Connected to server");
    };
    socket.onmessage = (event) => {
      const json = JSON.parse(event.data);
      console.log(json);
      console.log("====>", filterdUsers)
      if(activeuser && json.from == activeuser.id){
        console.log("====>", activeuser)
        activeuser.messages.push({
          "message": json.message,
          "media": JSON.stringify([]),
          "sender": json.from,
        });
      }
      if(filterdUsers){
        filterdUsers.forEach((x) => {
          if (x.id == json.from) {
            console.log("====>", x)
            x.messages.push({
              "message": json.message,
              "media": JSON.stringify([]),
              "sender": json.from,
            });
          }
        });  
      }

      // console.log(activeuser)

      

      // activeuser.messages.push({
      //   "message": json.message,
      //   "media": JSON.stringify([]),
      //   "sender": json.from,
      // })

      // setActiveUser({ ...activeuser, messages: activeuser.messages });

    };          
  }, []);



  useEffect(async () => {
    const filterData = await messages?.find((x) => x.id == activeuser?.id);
    if (activeuser) {
      setActiveUser(filterData);
      if (filterData.messages?.length > activeuser.messages.length) {
        bottomRef.current.scroll({
          top: bottomRef.current.scrollHeight,
          behavior: "smooth",
        });
      }
    }
  }, [messages]);



  useEffect(async() => {
    try{
      const res = await getGallery();
      if(res.data.success){
        setGalleryMedia(res.data.data)
      }
    }
    catch(error){

    }


    try {
      const res = await keywordsGet();
      if(res.data.status == 200) {
        setWords(res.data?.data)
      }
      else {
        toast.error(res.data.message);
      }
    }
    catch(error){
      toast.error('משהו השתבש');
    }
    
  },[])




  const getChat = async (first_render) => {   
    try {
      const res = await GetAllConnections();
      dispatch({type:SETMESSAGES, payload: res.data.data });
      
      if(first_render && chat___id){
        const filterData = await res.data.data.find((x) => x.connected_to_user.id == chat___id || x.connected_by_user.id == chat___id);
        // setActiveUser(filterData);
        setFilterdUsers(filterData);
      }
    } catch {}
  };


  useEffect(()=>{
    getChat(1);
  }, []);
    


  // setting connection to receive text and send text to target connection
  // const run = () => {
  //   var socket = io(`wss://ws.jojo.co.il/?token=${user?.token}`)
  //   console.log("HERE SOCKET ----")

  //   socket.emit("message", {
  //       command: "message",
  //       receiver: 251,
  //       connection_id: 219,
  //       message: 'text'
  //   }, (response) => {
  //     console.log("SENDING :", response)
  //   })
  // }

    // emit message
    // socket.on('message', (arg, callback) => {
    //     console.log('recv :', arg); // "world"
    //     callback("got it");
    // })



  
  //  274 1436|4ixOlkPo572rj4TNqjTJiycNhgErP58H7Z1J9vaL
  // useEffect(() => {
  //   console.log("======inbox===", user, user.id, user.token)
  //   // (`wss://ws.jojo.co.il/?token=1436|4ixOlkPo572rj4TNqjTJiycNhgErP58H7Z1J9vaL`)

  //   const socket = io("wss://ws.jojo.co.il/?token=jlkdajflkjadfk");

  //   // var socket = io.connect(`wss://ws.jojo.co.il/?token=1436|4ixOlkPo572rj4TNqjTJiycNhgErP58H7Z1J9vaL`)
  //   console.log("=== test ====", socket, typeof socket)

  //   // client-side
  //   socket.on("connect", () => {
  //     console.log(socket.id); // x8WIv7-mJelg7on_ALbx
  //   });

  //   socket.on("disconnect", () => {
  //     console.log(socket.id); // undefined
  //   });

    // socket.on('connect', function() {
    //   console.log("Connected to WS server");
    
    //   console.log(socket.connected); 
    
    // });


  //   function onConnect() {
  //     console.log("CONNECTing")
  //     setIsConnected(true);
  //   }

  //   function onDisconnect() {
  //     console.log("DISCONNECT")
  //     setIsConnected(false);
  //   }

  //   function onFooEvent(value) {
  //     setFooEvents(previous => [...previous, value]);
  //   }

  //   socket.on('connect', onConnect);
  //   socket.on('disconnect', onDisconnect);
  //   socket.on('foo', onFooEvent);

  //   return () => {
  //     socket.off('connect', onConnect);
  //     socket.off('disconnect', onDisconnect);
  //     socket.off('foo', onFooEvent);
  //   };
  // }, []);

  
    // console.log("HERE 223")
    // const WS_URL = 'ws://159.223.21.183:8081';
    // useWebSocket(WS_URL, {  
    //   onOpen: () => {
    //     console.log('WebSocket connection established.');
    //   }
    // });



  const sendChat = () => {    
    if(user__conection.paid===0 && user__conection.gender==="male" && user__conection.is_admin!==1){
      setModalView(true);
    }else{

      // alert(media.length);
      if(sendData.message === ""  && media.length === 0 && selectedMediaGallery.length === 0){
        return false;
      }
      
      if (words && words.length>0 && user__conection.is_admin!==1 ) {
        var filteredData = words.filter(function(obj) {
          return sendData.message.toLowerCase().includes(obj['keyword'].toLowerCase())
        });
        if (filteredData.length>0) {
          const for_remove = filteredData.map((x) => { return x.keyword }).toString();
          toast.error( `  מההודעה שלך  "${for_remove}" ${filteredData.length>1?'keywords':'keyword'} אנא הסר `);
          return;
        }
      }
            
      const rec_id = activeuser.connected_to_user?.id === user__conection.id
      ? activeuser.connected_by_user?.id
      : activeuser.connected_to_user?.id;

      // generate random unique id 26 chars
      const message_id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
      socket.send(JSON.stringify({
          "type":"message",
          "to":rec_id,
          "message":sendData.message,
          "message_id": message_id,
          "connection_id": activeuser.id,
      }))
      // setSendData({ ...sendData, message:""});
      // getChat();
      // setMedia([]);
      // setSelectedGallery([]);
      // setSelectedMediaGallery([]);
      // setSendData({ ...sendData, message:""});
      // getChat();

      // add message to chat
      activeuser.messages.push({
        "message": sendData.message,
        "media": JSON.stringify([]),
        "sender": user__conection.id,
      })

      setActiveUser({ ...activeuser, messages: activeuser.messages });

      setSendData({ ...sendData, message:""});

      // scroll to bottom
      
      // focus on last message

      return


      const formData = new FormData();
      formData.append('receiver',rec_id);
      formData.append('message',sendData.message);
      formData.append('connection_id', activeuser.id);

      if(media.length>0 || selectedMediaGallery.length>0){
        media.length>0 && media.map((item,i)=>{
          formData.append(`media[${i}]`,item);
        })

        selectedMediaGallery.length>0 && selectedMediaGallery.map((item,i)=>{
          if(media.length>0){
            formData.append(`media[${i===0?media.length:media.length+1}]`,item);
          }else{
            formData.append(`media[${i}]`,item);
          }
        })
      }

      console.log("words leng >0 and user connection is not admin :", sendData.message)
      dispatch(sendMessagesAction(formData));
      setMedia([]);
      setSelectedGallery([]);
      setSelectedMediaGallery([]);
      setSendData({ ...sendData, message:""});
      getChat();
    }
  };



  useEffect(() => {
    let searchResult = messages?.filter(
      (current_user) => {
      let res_user =
      current_user.connected_to_user?.id === user__conection?.id
      ? current_user.connected_by_user
      : current_user.connected_to_user;
      return res_user?.name.toLowerCase().indexOf(searchedValue.toLowerCase()) > -1
      }
    );
    setFilterdUsers(searchResult);
    if(activeuser){
      setActiveUser(searchResult.find((x)=>x.id===activeuser.id));
    }
  },[searchedValue,messages])


  const allowedFileTypes = ["jpg", "JPG", "PNG", "WEBP", "png", "jpeg", "webp"];
  const setMediaFormate = async (images) => {
    let array_of_images = [];
    await Array.from(images).forEach(file => { 
      var file_size = file.size;
      var file_extension = file.name.split('.').pop();
      if (allowedFileTypes.includes(file_extension)) {

        if(file_size > 10240000){
          toast.error("אפשר עד 10MB");
        }else{
          array_of_images.push(file) 
          setMedia(media.concat(array_of_images))
        }

      }else{
        toast.error("לקבל רק תמונות");
      }
    })
  }


  const setMessage = (msg) => {
    setSendData({...sendData, message: msg})
  }


  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })


  const DeleteMessages = async () => {
    const name =  activeuser?.connected_to_user?.id === user__conection.id?
    activeuser?.connected_by_user?.name:
    activeuser?.connected_to_user?.name;
    Swal.fire({
      html: lang.deleteCnfrm,
      timerProgressBar: true,
      showCancelButton: true,
      confirmButtonText: lang.delete_it,
      confirmButtonColor: '#d7a940',
      cancelButtonText: lang.no_cancel,
      reverseButtons: true,
      icon:"question"
    }).then(async(result) => {
      if (result.isConfirmed) {
        Swal.showLoading()
        try{
          const res = await deleteUserChat({connection_id:activeuser?.id});
          if(res.data.success){
            dispatch({ type: SETMESSAGES, payload: res.data.data });
            toast.success(res.data.message);
          }else{
            toast.error(res.data.message);
          }
        }
        catch(error){
          toast.error("משהו השתבש");
        }
        Swal.hideLoading()
        swalWithBootstrapButtons.fire(
          lang.deleted,
          lang.chat_has_been_deleted,
          'success'
        )
      } else if (
        result.dismiss === Swal.DismissReason.cancel
      ) {
     
      }
    })
  }


  const storeSelectedGallery = (item) => {
    if(selectGallery.find((x)=>x.id===item.id)){
      setSelectedGallery(selectGallery.filter((x)=>x.id!==item.id));
      return;
    }
    setSelectedGallery(selectGallery.concat(item));
  }


  const saveSelectedGalleryEvent = () => {
    const gall = selectGallery.map((item)=>{return item.image})
    setSelectedMediaGallery(gall);
    setModalShowGallery(false);
  }
  
  const style = langauge === 'heb' ? {fontSize:"13px",fontWeight:600, float: "right" } : {fontSize:"13px",fontWeight:600};
  
  
  const UploadGallery = async (file) => {
    if(!file){
      // toast.error('Please upload minimum 1 picture');
      return;
    }
    setDelLoader(true);
    const formData = new FormData();
    formData.append(`media[0]`,file);
    try{       
      const res = await uploadGalleryAPi(formData);
      if(res.data.success){
        toast.success(res.data.message);
        setGalleryMedia(res.data.data.gallery)
      }else{
        toast.error(res.data.message);
      }
    }
    catch(error){
      toast.error('משהו השתבש');
    }
    setDelLoader(false);
  }


  const deleteGalleryImage = async (id,t) => {
    setDelLoader(true)
    try{
      const res = await galleryDeleteApi({gallery_id:id})
      if(res.data.success){
        setGalleryMedia(galleryMedia.filter((x)=>x.id!==id))
        setSelectedGallery(selectGallery.filter((x)=>x.id!==id))
        setSelectedMediaGallery(selectedMediaGallery.filter((x,i)=>i!==t))
      }else{
        toast.error(res.data.message);
      }
    }
    catch(error){

    }
    setDelLoader(false)
  }


  const handleSetMessage = (e) =>{
    setMessage(e.target.value)
  }


  const handleKeyPressed = (e) =>{
    if (e.key === "Enter") {
      sendChat()
    }
  }


  return (
    <>
      <InboxWrapper type={user__conection.is_admin} >
        <div className="container_ h-100 w-100">
          <div className="wrapper h-100 w-100">
            <div className="head d-flex align-items-center flex-lg-nowrap flex-wrap justify-content-between">
              <h1 className="m-0 text-black text-capitalize">{lang.messages}</h1>
              <div className="search d-flex align-items-center">
                <input type="text" placeholder={lang.search} onChange={(e)=>setSearchedValue(e.target.value)} />
                <img src={searchIcon} alt="search icon" />
              </div>
            </div>
            <div className="body_ d-flex">
              <div className="users-bar h-100">
                <h6>{lang.chat_users}</h6>
                <div className="online-user-divider"></div>
                {filterdUsers && filterdUsers.length>0 && filterdUsers.map((list, key) => {
                  let count_unread = list.messages.filter((x) => x.status === 0);
                  let res_user =
                  list.connected_to_user?.id === user__conection?.id
                  ? list.connected_by_user
                  : list.connected_to_user;
                  return (
                    <div
                      className={`recent-user d-flex ${list.id===activeuser?.id?"active":""}`}
                      key={key}
                      onClick={async() => {
                        await setActiveUser(list);
                        seenMessgaeApi(list);
                        bottomRef.current.scroll({
                          top: bottomRef.current.scrollHeight,
                          behavior: "smooth",
                        });
                      }}
                    >
                      <div className={`user-avatar position-relative ${res_user?.online_status=="1"?"online":"offline"}`}>
                        <Image src={userAvatar(res_user)} alt="user" onError={(e) => e.target.src = config.backEnd+"/default-user.png"}/>
                      </div>
                      <div className="text-info d-flex align-items-center justify-content-between flex-grow-1">
                        <div className="flex-grow-1">
                          <h2 className="text-capitalize">{res_user?.name}</h2>
                          {list.messages?.length > 0 ? (
                            <p>
                              {list.messages[list.messages?.length - 1].message}
                            </p>
                          ) : null}
                        </div>
                        {list.unread_messages > 0 ? 
                          <span className="unread d-flex align-items-center justify-content-center">
                            {list.unread_messages}
                          </span> : ''}
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className={`chat-box h-100 ${media.length>0?"active_media":null} ${activeuser?.id?"active":""}`}>
                <div className="head d-flex align-items-center justify-content-between">
                  <i className={`ri-arrow-${langauge==="heb"?"right":"left"}-s-line d-md-none`} onClick={()=>setActiveUser(null)} style={{fontSize:"30px"}}></i>
                  <h2 className="d-flex flex-md-row flex-column align-item-center text-black text-center mb-0 text-capitalize mx-md-0 mx-auto">
                    <span>
                      {activeuser ? (
                        <Link to={`/profile/${activeuser?.connected_to_user?.id}`}>
                          <img
                            src={userAvatar(activeuser?.connected_to_user?.id === user?.id?activeuser.connected_by_user:activeuser?.connected_to_user)}
                            alt="user avatar"
                            className="avatar_"
                            onError={(e) => e.target.src = config.backEnd+"/default-user.png"}
                          />
                        </Link>
                      ) : null}
                    </span>
                    {activeuser ? (
                    <Box className='d-flex align-items-center justify-content-center'>
                      <span className="user_text">
                        {
                          activeuser?.connected_to_user?.id === user__conection?.id?
                          activeuser?.connected_by_user?.name:
                          activeuser?.connected_to_user?.name
                        }
                      </span>
                      {/* <OverlayTrigger> */}
                      {/* <img
                        src={userAvatar(activeuser?.connected_to_user?.id === user__conection?.id?activeuser.connected_by_user:activeuser?.connected_to_user)}
                        alt="user avatar"
                        className="avatar_ d-md-block d-none"
                      /> */}
                      {/* </OverlayTrigger> */}
                    </Box>
                    ):null}
                  </h2>
                  {activeuser ? (
                     <button className='border-0 p-0 delete_messages_btn bg-transparent' disabled={del_loader} onClick={()=>DeleteMessages()}>
                     <img src={Trash} width="25px" alt="delete messages" />
                   </button>
                   
                  ) : null}
                </div>
                {activeuser ? (
                  <>
                    <div className="body" id="chat_body" ref={bottomRef}>
                      {/* <p className="time text-center">Today at 5:03 PM</p> */}
                      {activeuser?.messages?.map((list, key) => {
                        return (
                          <h6
                            className={`${
                              list.sender !== user__conection.id ? "inbound" : "outbound"
                            } message`}
                            key={key}
                          >
                            {list?.media && JSON.parse(list.media).length>1?
                              <div className="row">
                                {JSON.parse(list.media).map((item,i)=>{
                                  return(
                                    <div className="col-6 mb-3" key={i}>
                                      <a href={`${IMAGE_URL}/${item}`}  target="_blank" download>
                                        <Image style={{height:"200px",objectFit:"contain",borderRadius:"10px"}} src={`${IMAGE_URL}${item}`} />
                                      </a>
                                    </div>
                                  )})}
                                  <br /><br />
                              </div>
                              :list?.media && JSON.parse(list.media).length===1?
                                <div className={`${list.message?"mb-3":"mb-0"}`}>
                                <a href={`${IMAGE_URL}${JSON.parse(list.media)[0]}`}  target="_blank" download>
                                  <Image style={{height:"200px",objectFit:"contain",borderRadius:"10px"}} src={`${IMAGE_URL}${JSON.parse(list.media)[0]}`} />
                                </a>
                              </div>
                            :null}
                            {list.message}
                          </h6>
                        );
                      })}
                      {/* <p className="time text-center">Today at 5:03 PM</p> */}
                    </div>
                    <div className="footer d-flex">
                      <Dropdown align={langauge==="heb"?"end":"start"}>
                        <Dropdown.Toggle variant="none" bsPrefix="custom border-0 attach-border-m p-0 w-auto" style={{minWidth:0,background:'transparent'}}>
                          <label className="attach-file border-0 center" type="button">
                            <img src={attachPin} alt="icon" />
                          </label>
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                          <Dropdown.Item href="javascript:void(0)" className="p-0" onClick={()=>setModalShowGallery(true)}>
                            <label style={style} className="px-2 py-1 d-block" type="button">
                              {lang.select_from_gallery}
                            </label>
                          </Dropdown.Item>
                          <Dropdown.Item href="javascript:void(0)" className="p-0">
                            <label style={style} className="px-2 py-1 d-block" type="button">
                              {lang.upload_from_device}
                              <input type="file" accept="image/*" className="d-none"  multiple  onChange={(e) => setMediaFormate(e.target.files)} />
                            </label>
                          </Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                      <div className="d-flex flex-grow-1 message-input-wrapper">
                        <div className="flex-grow-1">
                          {media.length>0 || selectedMediaGallery.length>0?
                            <div className="output-result">
                              <ul className="d-flex list-unstyled mb-0 list-inline">
                                {media.length>0 && media.map((item,key)=>{
                                  return(
                                    <li key={key} className="list-inline-item position-relative">
                                      <i className="ri-close-circle-fill" onClick={()=>setMedia(media.filter((x,i)=>i!==key))} style={{fontSize:"20px", color: "Gray"}} type="button"></i>
                                      <img height="58px" src={URL.createObjectURL(item)} alt="Result Image" />
                                    </li>
                                  )
                                })}
                                {selectedMediaGallery.length>0 && selectedMediaGallery.map((item,key)=>{
                                  return(
                                    <li key={key} className="list-inline-item position-relative">
                                      <i className="ri-close-circle-fill" onClick={()=>{setSelectedMediaGallery(selectedMediaGallery.filter((x,i)=>i!==key));setSelectedGallery(selectGallery.filter((x,i)=>i!==key))}} style={{fontSize:"20px", color: "Gray"}} type="button"></i>
                                      <img height="58px" src={ASSET_URL+item} alt="Result Image" />
                                    </li>
                                  )
                                })}
                                <li>
                                  <Dropdown>
                                    <Dropdown.Toggle variant="none" bsPrefix="dropdown-custom border-0 attach-border-m p-0 w-auto" style={{minWidth:0,background:'transparent'}}>
                                      <label className="file-upload-gallery d-flex align-items-center justify-content-center">
                                        <i className="ri-add-circle-fill position-static"></i>
                                      </label>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                      <Dropdown.Item href="javascript:void(0)" className="p-0" onClick={()=>setModalShowGallery(true)}>
                                        <label style={{fontSize:"13px",fontWeight:600}} className="px-2 py-1 d-block" type="button">
                                          Select from Gallery
                                        </label>
                                      </Dropdown.Item>
                                      <Dropdown.Item href="javascript:void(0)" className="p-0">
                                        <label style={{fontSize:"13px",fontWeight:600}} className="px-2 py-1 d-block" type="button">
                                          Upload from device
                                          <input type="file" accept="image/*" className="d-none"  multiple  onChange={(e) => setMediaFormate(e.target.files)} />
                                        </label>
                                      </Dropdown.Item>
                                    </Dropdown.Menu>
                                  </Dropdown>
                                </li>
                              </ul>
                            </div>:null}

                          {/* <InputEmoji value={sendData.message} onChange={(txt)=>setMessage(txt)} onEnter={sendChat} placeholder={lang.type_message}/> */}
                          {/* // onChange={(e) => setSendData({ ...sendData, message:e.target.value })} */}
                          <InputEmoji
                            className={`text-rtl`}
                            value={sendData.message}
                            onChange={(txt)=>setMessage(txt)}
                            onEnter={sendChat}
                            placeholder={lang.type_message}
                            keepOpened 
                          />

                          {/* <input className={`send-msg-input-element`} type="text" onChange={handleSetMessage}
                            onKeyPress={handleKeyPressed}
                            value={sendData.message}
                            // onChange={(txt)=>setMessage(txt)}
                            // onEnter={sendChat}
                            placeholder={lang.type_message}
                            keepOpened 
                          /> */}

                        </div>
                        <button
                          className="border-0 circle__btn"
                          onClick={() => sendChat()}
                        >
                          <img src={rightArrow} alt=">" />
                        </button>
                      </div>
                    </div>
                  </>
                ) : (
                  <Heading className="text-center py-5" size={20}>
                    <Box className="d-block">
                      <i
                        className="ri-message-2-fill"
                        style={{ fontSize: "50px", color: "#006aff" }}
                      ></i>
                    </Box>
                    {lang.click_start_chat}
                  </Heading>
                )}
              </div>
            </div>
          </div>
        </div>
      </InboxWrapper>
      {modalView?
        <UpgradePremiumModel onModalCancel={setModalView} />
      :null}
      <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        show={modalShowGallery}
        onHide={() => setModalShowGallery(false)}
        centered
        backdrop="static"
      >
        <Modal.Header closeButton={!del_loader}>
          <Modal.Title id="contained-modal-title-vcenter" style={{fontSize:"20px",fontWeight:700}}>
            {user__conection?.name}'s {lang.gallery}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="user-side-wrap mx-0">
          <Row className="user-listing pb-0 d-flex pt-0 mx-0">
            {galleryMedia.length>0 && galleryMedia.map((item,key)=>{
              const active_data = item.id && selectGallery.length> 0 &&
              selectGallery.find((x)=>x.id===item?.id);
              return(
                item?.image?
                  <>
                    <Col lg={3} md={4} xs={6} key={key} className="mb-3 gallery__data gallery___item">
                      <Box className={`user-card h-200 position-relative m-0${active_data?" selected":''}`} onClick={()=>storeSelectedGallery(item)}
                        style={{height:"200px !important",flexBasis:"auto",maxWidth:"inherit",cursor:"pointer"}}>
                        <Image
                          className="w-100"
                          src={IMAGE_URL+item.image}
                          style={{
                            height: "100%",
                            maxHeight: "initial",
                            objectFit:"cover"
                          }}
                        />
                        {!del_loader?
                          <i className="ri-close-circle-fill text-secondary position-absolute" 
                            onClick={()=>deleteGalleryImage(item.id,key)} type="button">
                          </i>
                        :null}
                      </Box>
                    </Col>
                  </>
                :null
              )
            })}
            <Col lg={3} md={4} xs={6} className="mb-3 gallery__data gallery___item">
              <FileUploaderGallery>
                <Box className='user-card bg-transparent m-0' style={{flexBasis:"auto",maxWidth:"inherit",cursor:"pointer"}}>
                  <label className="file-upload-gallery w-100 d-flex align-items-center justify-content-center">
                    <i className="ri-add-circle-fill position-static"></i>
                    <input accept="image/*" onChange={(e)=>UploadGallery(e.target.files[0])} className="d-none" type="file" />
                  </label>
                </Box>
              </FileUploaderGallery>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button className="border-0" disabled={del_loader} onClick={()=>saveSelectedGalleryEvent()} style={{color:"#212529"}}>{lang.select}&nbsp;({selectGallery.length})</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};


const FileUploaderGallery = styled.div`
  .user-card{
    overflow:hidden;
    .file-upload-gallery{
      cursor:pointer;
      height:196px;
      i{
        font-size:50px;
        color:#D39B2C;
      }
    }
    span{
      height:100%;
    }
    &.bg-transparent{
      border:3px dashed #bfbfbf;
      &:before{
        display:none;
      }
    }
    .lazy-load-image-background{
      width:100%;
      height:200px;
      img{
         object-position:top center;
      }
    }
  }
`
export const InboxWrapper = styled.div`
  .user_text {
    font-size: 16px;
    margin: 0 0 0 8px;
  }
  .emoji-mart{
    width:100% !important;
  }
  .active_media #chat_body{
    padding:15px 20px 57px;
  }
  .file-upload-gallery{
    width:50px;
    height:58px;
    border:1.5px dashed #9a9a9a;
    cursor:pointer;
    i{
      color:#d39b2c;
    }
  }
  .output-result{
    background: #f1f3f6;
    position: absolute;
    bottom: 31px;
    width: 100%;
    padding: 10px 20px 13px;
    border-top-left-radius:25px;
    border-top-right-radius:25px;
    ul{
      border-bottom: 1px solid #d1d1d1;
      margin-left: -4px;
      margin-right: -4px;
    }
    i{
      font-size: 20px;
      position: absolute;
      top: -10px;
      left: -7px;
      z-index: 1;
    }
    li{
      margin-bottom:5px;
    }
    img{
      border-radius:4px;
    }
    @media(max-width:767px){
      padding:10px 10px;
      .file-upload-gallery{
        height:40px;
        width:40px;
      }
      img{
        height:40px;
      }
    }
  }
  @media(min-width: 992px){
    height: calc(100vh - 121px);
    padding: ${(props)=> props.type === 1 ? '20px 0px' : '20px 30px' };
  }
  @media(max-width: 991px){
    height: calc(100vh - 80px);
    padding: 20px 15px;
  }
  @media(min-width:991px){
    background-color: ${(props)=> props.type === 1 ? 'transparent' : '#f1f3f6' };
  }
  .container_ {
    margin-inline: auto;
    max-width: 1446px;
    .wrapper {
      height: 100%;
      width: 100%;
      border-radius: 8px;
      @media(min-width: 992px){
        background: #ffffff;
        box-shadow: 0px 12px 250px rgba(0, 0, 0, 0.1);
      }
      .head {
        @media(min-width: 992px){
          height: 70px;
          border-bottom: 1px solid #c0bec7;
          padding: 13px 13px 13px 28px;
        }
        @media(max-width: 991px){
          padding: 15px 0px;
        }
        h1 {
          font-weight: 700;
          font-size: 24px;
          line-height: 29px;
          @media(max-width: 991px){
            font-size: 40px;
          }
        }
        .search {
          width: 370px;
          height: 42px;
          background: #f1f3f6;
          border-radius: 8px;
          padding-right: 10px;
          @media(max-width: 991px){
            margin-top: 30px;
          }
          input {
            background-color: transparent;
            border: 0;
            height: 100%;
            flex: 1;
            padding: 0 0 0 15px;
            font-weight: 400;
            font-size: 16px;
            line-height: 19px;
            color: #000000;
            &::placeholder {
              color: grey;
            }
          }
          img {
            width: 20px;
          }
        }
      }
      .body_ {
        height: calc(100% - 69px);
        .users-bar {
          @media(min-width:992px){
            border-right: 1px solid #c0bec7;
          }
          @media(max-width:991px){
           padding-left: 0px !important;
           padding-right:0px !important;
           ::-webkit-scrollbar{
            display:none !important;
           }
          }
          padding: 20px 28px;
          width: 390px;
          overflow-y: auto;
          scrollbar-width: thin;
          ::-webkit-scrollbar {
            width: 8px;
          }
          ::-webkit-scrollbar-track {
            background: #f1f3f6;
          }
          ::-webkit-scrollbar-thumb {
            background: #c0bec7;
          }
          h6 {
            font-weight: 600;
            font-size: 16px;
            line-height: 19px;
            color: #9a9a9a;
            margin-bottom: 15px;
          }
          .user-avatar {
            min-width: 56px;
            width: 56px;
            height: 56px;
            span {
              width: 100%;
              height: 100%;
            }
            img {
              width: 100%;
              height: 100%;
              border-radius: 50%;
              object-fit: cover;
              object-position: top;
            }
            &::after {
              content: "";
              width: 13px;
              height: 13px;
              border-radius: 50%;
              position: absolute;
              bottom: 2px;
              right: 1px;
            }
            &.online::after {
              background: #7fd31f;
            }
            &.offline::after {
              background: #c0bec7;
            }
          }
          h5 {
            font-weight: 500;
            font-size: 13px;
            line-height: 17px;
            text-align: center;
            color: #000000;
            margin: 7px 0 0;
          }
          .online-user-divider {
            margin: 20px 0;
            border-bottom: 1px solid #f1f3f6;
            width: 100%;
          }
          .recent-user {
            margin-bottom: 20px;
            cursor: pointer;
            &.active{
              .text-info {
                border-color: #d39b2c !important;
              }
            }
            .text-info {
              border-bottom: 1px solid #f1f3f6;
              margin-left: 15px;
              padding-bottom: 10px;
              h2 {
                font-weight: 600;
                font-size: 18px;
                line-height: 22px;
                margin-bottom: 4px;
                color: #000000;
              }
              p {
                font-size: 15px;
                line-height: 19px;
                color: #9a9a9a;
                margin: 0;
                white-space: nowrap;
                text-overflow: ellipsis;
                overflow: hidden;
                width: 200px;
              }
              .unread {
                background: linear-gradient(180deg, #dbae2a 0%, #cf8d2d 100%);
                width: 28px;
                height: 28px;
                border-radius: 50%;
                font-weight: 500;
                font-size: 14px;
                line-height: 19px;
                text-align: center;
                color: #ffffff;
              }
            }
          }
        }
        .chat-box {
          @media(max-width:991px){
            &:not(.active){
              display: none;
            }
            position: fixed;
            top: 0;
            z-index: 99;
            background: #fff;
            width: 100%;
            left: 0;
          }
          flex: 1;
          .head {
            height: 75px;
            padding: 15px;
            border-color: #f1f3f6;
            box-shadow: 0 1px 9px -2px #c0c0c0;
            h2 {
              font-weight: 700;
              font-size: 20px;
              line-height: 24px;
            }
            .avatar_ {
              width: 45px;
              height: 45px;
              border-radius: 50%;
              object-fit: cover;
            }
          }
          .body {
            padding: 15px 20px;
            height: calc(100% - 150px);
            overflow-y: auto;
            scrollbar-width: thin;
            ::-webkit-scrollbar {
              width: 8px;
            }
            ::-webkit-scrollbar-track {
              background: #f1f3f6;
            }
            ::-webkit-scrollbar-thumb {
              background: #c0bec7;
            }
            .time {
              color: #c0bec7;
              font-size: 13px;
              margin-bottom: 15px;
              clear: both;
            }
            .message {
              margin-bottom: 15px;
              padding: 15px;
              font-weight: 400;
              font-size: 14px;
              line-height: 17px;
              color: #000000;
              max-width: 500px;
              min-width: 110px;
              clear: both;
              position: relative;
              &::before {
                content: "";
                position: absolute;
                top: 0;
                width: 0;
                height: 0;
              }
              &.inbound {
                float: left;
                background: #dae6ff;
                border-radius: 0px 8px 8px 8px;
                &::before {
                  left: -9px;
                  border-top: 0 solid transparent;
                  border-bottom: 10px solid transparent;
                  border-right: 10px solid #dae6ff;
                }
              }
              &.outbound {
                float: right;
                background: #efefef;
                border-radius: 8px 0 8px 8px;
                &::before {
                  right: -9px;
                  border-top: 10px solid #efefef;
                  border-bottom: 10px solid transparent;
                  border-right: 10px solid transparent;
                }
              }
            }
          }
          .footer {
            height: 65px;
            padding: 10px 15px 15px;
            width: 100%;
            .react-input-emoji--container{
              background:transparent;
              margin-left:0px;
              margin-right:0px;
              border:0;
            }
            .react-input-emoji--input{
              font-size:13px;
              max-height:39px;
              word-break: break-all;
            }
            .attach-border-m{
              margin-right:10px;
              .attach-file {
                width: 50px;
                height: 50px;
                background: #d39b2c;
                border-radius: 50%;
                position: relative;
                flex-shrink: 0;
                input {
                  position: absolute;
                  top: 0;
                  left: 0;
                  width: 100%;
                  height: 100%;
                  opacity: 0;
                }
              }
            }
            .message-input-wrapper {
              background: #f1f3f6;
              border-radius: 25px;
              height: 50px;
              input {
                font-weight: 400;
                font-size: 14px;
                line-height: 17px;
                color: #000;
                padding-left: 20px;
                &&::placeholder {
                  color: #c0bec7;
                }
              }
              button.circle__btn {
                width: 50px;
                flex-shrink: 0;
                height: 50px;
                background: #d39b2c;
                border-radius: 50%;
                &:disabled{
                  opacity:0.7;
                }
              }
            }
          }
        }
      }
    }
  }
  @media (max-width: 768px) {
    .user_text {
      font-size: 11px;
      margin: 0;
    }
  }
`;
export default React.memo(Inbox);
