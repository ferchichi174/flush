import React, { useEffect, useState , useRef } from "react";
import { Link } from "react-router-dom";
import {  Dropdown } from "react-bootstrap";
import {
  PurchaseModel,
  MatchModel,
  Notification,
  RateModal,
  LocationeModel,
  CameraModel,
} from "../components/Ui/Popups";
import { useLocation } from "react-router-dom";
import { GetRandomUsers } from "../constant/api/Api";
import { useSelector, useDispatch } from "react-redux";
import { UsersRandomalyAction } from "../redux/actions/ClientActions";
import { Image, LogoSite, MobileScreenFilter, SpinnrLoader } from "../components/Components";
import { getAge, userAvatar } from "../helpers/helper";
import { Box, Heading, Input, Label } from "../components/Style";
import "react-rangeslider/lib/index.css";
import { toast } from "react-toastify";
import { citiesOptions } from "./auth/Register";
import { FilterIcon } from "../components/AllImages";
import config from '../config.json';

const Home = ({ lang }) => {
  const { search } = useLocation();
  const { randomUsers } = useSelector((state) => state.clients);
  const { langauge } = useSelector((state)=>state.clients);
  const { user } = useSelector((state) => state.auth);
  const [all_users, setAllUsers] = useState(randomUsers);
  const [activeTab,setActiveTab]= useState({id:'All user',name:lang.tabs.all_user,langauge_name:"all_user"});
  const [age_options, setAgeOptions] = useState([]);
  const [max_age_options, setMaxAgeOptions] = useState([]);
  const [cityOptions,setCityOptions]=useState([]);
  const [dropdownShow,setDropdownShow] = useState(false)
  const [onlineUsers, setOnlineUsers] = useState([]);
  const [onlineUsersInterval, setOnlineUsersInterval] = useState(null);
  const [filter_search, setFilterSearch] = useState({
    search: "",
    // looking_for: "",
    birth_date: 0,
    city: "",
    birth_max_date: 0,
  });
  const dropdownRef = useRef();
  const dispatch = useDispatch();
  const [loader, setLoader] = useState(true);
  const [socket, setSocket] = useState(null);
  const socketRef = useRef(null);
  const [socketConnectInterval, setSocketConnectInterval] = useState(null);

  const socketConnect = () => {

    const token = localStorage.datingAppauthToken;
    if(token){
      let tmpSocket = new WebSocket(`${config.websocket}?token=${token}`);
      tmpSocket.onopen = (connection) => {
        console.log("Connected to server");
        setOnlineUsersInterval(setInterval(() => {
          tmpSocket.send(JSON.stringify({type: 'online_users'}));
        }, 10000));
        clearInterval(socketConnectInterval);
        setSocketConnectInterval(null);
        tmpSocket.send(JSON.stringify({type: 'online_users'}));
      };
      tmpSocket.onerror = (error) => {
        console.log("Error in connection establishment", error);
        // wait 5 seconds and try again
        setSocketConnectInterval(setInterval(function () {
          socketConnect();
        }, 5000));
      };
      tmpSocket.onclose = (event) => {
        console.log("Socket Closed Connection: ", event);
        // wait 5 seconds and try again
        setTimeout(function () {
          socketConnect();
        }
        , 5000);
      };
      tmpSocket.onmessage = (event) => {
        let data = JSON.parse(event.data);
        if(data.type === "online_users"){
          setOnlineUsers(data.users);
        }
      }
      setSocket(tmpSocket)
      // update SocketRef
      socketRef.current = tmpSocket;
    }else{
      // wait 10 seconds and try again
      setTimeout(function () {
        socketConnect();
      }, 10000);
    }  
  }

  useEffect(() => {
    async function fetchData() {
      setLoader(true);
      await dispatch(UsersRandomalyAction(GetRandomUsers, setAllUsers));
      for (let x = 18; x < 100; x++) {
        const option = { label: x , value: x };
        setAgeOptions((prev) => [...prev, option]);
        if (x !== 18) {
          setMaxAgeOptions((prev) => [...prev, option]);
        }
      }
      setLoader(false);
    }
    fetchData();
    socketConnect();
    
  }, []);

  useEffect(()=> {
    if(activeTab.id=='Online')
      setAllUsers( randomUsers.filter(( x ) => x.online_status == 1 ) )
    else
      setAllUsers( randomUsers )
  },[activeTab])
  useEffect(()=>{
    setActiveTab({id:activeTab.id,name:lang.tabs[activeTab.langauge_name],langauge_name:activeTab.langauge_name})
  },[langauge])
  useEffect(() => {
    citiesOptions(lang,setCityOptions);
  },[langauge])


  const user_alt_gender = user?.gender=="male"?"female":"male";
  const get_filter_result = () => {
    let obj_count = 0;
      Object.keys(filter_search).forEach((item, i) => {
        if (filter_search[item] && item !== "search") obj_count += 1;
      });
      if (randomUsers.length > 0) {
        const result = (activeTab.id=='Online'? randomUsers.filter( (x) => x.online_status == 1 ) : randomUsers)
          .filter((x) => x.id !== user.id && x.gender!==user?.gender)
          .filter((imp) => {
            let data_count = 0;
            // if (
            //   imp.gender &&
            //   imp.gender == filter_search.looking_for
            // ) {
            //   data_count += 1;
            // }
            if (
              imp.birth_date &&
              getAge(imp.birth_date) >= filter_search.birth_date &&
              getAge(imp.birth_date) <= filter_search.birth_max_date
            ) {
              data_count += 2;
            }
            if (imp.city && imp.city == filter_search.city) {
              data_count += 1;
            }
            // console.log(obj_count + " " + data_count);
            return obj_count == data_count;
          });
        setAllUsers(result);
        const resultData = result;
        // resultData.length>0?toast.success(`${resultData.length} ${resultData.length==1?"User":"Users"} Matched`):toast.error(`${resultData.length} User Matched`);
      }
  }


  useEffect(()=> {
    const res = activeTab.id=='Online'? randomUsers.filter( (x) => x.online_status == 1 ) : randomUsers
    let searchResult = res.filter(
      (current_user) =>
      current_user.name.toLowerCase().indexOf(filter_search.search.toLowerCase()) >
      -1
    );
    setAllUsers(searchResult);
  },[filter_search.search,randomUsers])

  const TriggerFilter = () => {
    get_filter_result();
  }
  const style = langauge === 'eng' ? { direction: "rtl" } : {};
  const resetData = () => {
    const result = randomUsers.filter((x) => x.id !== user.id);
    setFilterSearch({
      ...filter_search,
      search: "",
      // looking_for: "",
      birth_date: "",
      city: "",
      birth_max_date: "",
    });
    setAllUsers(result);
  };
  useEffect(() => {
    window.addEventListener('click',(event)=> {
      if(dropdownRef.current !== null){
        // console.log("====>", dropdownRef.current);
        if (!dropdownRef.current?.contains(event.target)) {
          setDropdownShow(false);
        }
      }
    });
  }, []);
  return (
    <>
      <main>
        {search === "?purcahse" ? <PurchaseModel /> : null}
        {search === "?match" ? <MatchModel /> : null}
        {search === "?notification" ? <Notification /> : null}
        {search === "?location" ? <LocationeModel /> : null}
        {search === "?camera" ? <CameraModel /> : null}
        {search === "?rate" ? <RateModal /> : null}
        <Box className="container px-0">
          {(user.profile_updated === 0) && 
            <div class="alert alert-success" role="alert" style={{textAlign:"end"}}>
              ברוכים הבאים לג'וג'ו! שמנו לב שעדיין לא העלית תמונת פרופיל. משתמשים עם תמונה מקבלים פי 7 פניות ותשובות להודעות שלהם באתר. <Link style={{color:"blue"}} to={`/profile/${user.id}`}>לחץ כאן כדי להעלות תמונה ולערוך את הפרופיל</Link>
            </div>
          }
          {randomUsers && randomUsers.length === 0 ? (
            loader ? (
              <Box className="text-center py-5">
                <SpinnrLoader color="#000" />
              </Box>
            ) : null
          ) : null}
          <div className="px-0">
            <Box className="card card__responsive" style={{overflow: "visible"}}>
              <Box className="logo color pt-4 mb-2 text-center d-none">
                <LogoSite height="55px" />
              </Box>
              <Box className="card-header res_head d-block border-md-bottom d-md-flex">
                <Box className="w-100" style={{ paddingLeft: "3px", paddingRight: "3px" }}>
                  <Box className="d-flex align-items-center align-items-md-center">
                    <h5 className={`page-content-name mb-0 ${langauge=="eng" || !langauge ?"me-md-5":"ms-md-5"}`}>{activeTab.name}</h5>
                    <Box className="filter ms-0 flex-1 position-static position-md-relative">
                      <Dropdown className="position-static position-md-relative" ref={dropdownRef} show={dropdownShow} autoClose={"outside"} >
                        <Dropdown.Toggle variant="none" className="ps-0 mb-0 text-capitalize d-md-flex d-none custom__dropdown__filter" onClick={()=>setDropdownShow(!dropdownShow)}><i className="ri-equalizer-line"></i>&nbsp;&nbsp;{lang.filter_users}</Dropdown.Toggle>
                        <Dropdown.Toggle variant="none" className="custom__dropdown__filter p-0 bg-transparent d-md-none" bsPrefix="button" onClick={()=>setDropdownShow(!dropdownShow)}>
                          <FilterIcon width="55px" height="55px" />
                        </Dropdown.Toggle>
                        <Dropdown.Menu className="w-100 px-3 pt-3">
                          <Box className="text-end mb-2">
                            <button type="button" className="bg-transparent border-0" style={{fontWeight:600,fontSize:"30px",marginTop:"-18px",marginRight:"-11px", color: "Gray"}} onClick={()=>setDropdownShow(false)}>
                              <i className="ri-close-circle-fill"></i>
                            </button>
                          </Box>
                          {/* <LargeScreenFilter lang={lang} setFilterSearch={setFilterSearch} 
                            filter_search={filter_search}
                            age_options={age_options} max_age_options={max_age_options} cityOptions={cityOptions}
                          /> */}
                          <MobileScreenFilter lang={lang} setFilterSearch={setFilterSearch} 
                            filter_search={filter_search}
                            age_options={age_options} max_age_options={max_age_options} cityOptions={cityOptions}
                          />
                          <Box className="card-footer justify-content-between align-items-center d-flex mt-4 pt-3 bg-transparent">
                            <button
                              type="button"
                              className="border-0 bg-transparent mt-2  d-flex align-items-center"
                              onClick={() => resetData()}
                            >
                              <i className="ri-restart-line" style={{ fontSize: "20px" }}></i>
                              &nbsp;
                              <span style={{ fontSize: "16px" }}>{lang.reset_all}</span>
                            </button>
                            <a href="#" onClick={() => {TriggerFilter(); setDropdownShow(false)}} className="btn text-capitalize">
                              {lang.apply_filter}
                            </a>
                          </Box>
                        </Dropdown.Menu>
                      </Dropdown>
                    </Box>
                    {/* <Box className="position-relative flex-1 d-md-block d-none">
                      <Input
                        type="text"
                        placeholder={lang.search_name}
                        value={filter_search.search}
                        style={{ paddingLeft: "40px" }}
                        onChange={(e) =>
                          setFilterSearch({
                            ...filter_search,
                            search: e.target.value,
                          })
                        }
                      />
                      <i
                        className="ri-user-search-line"
                        style={{
                          fontSize: "18px",
                          position: "absolute",
                          top: "10px",
                          left: "10px",
                        }}
                      ></i>
                    </Box> */}
                  </Box>
                </Box>
              </Box>

              {/* <Box>
                <ul className="d-flex justify-content-between mb-3 tabs-filters__" style={style}>
                  <li>
                    <a href="#" onClick={()=>setActiveTab({id:'New',name:lang.tabs.new,langauge_name:"new"})} className={`${activeTab.id=="New"?"active":""}`}>
                      {lang.tabs.new}
                    </a>
                  </li>
                  <li>
                    <a href="#" onClick={()=>setActiveTab({id:'Online',name:lang.tabs.online,langauge_name:"online"})} className={`${activeTab.id=="Online"?"active":""}`}>
                      {lang.tabs.online}
                    </a>
                  </li>
                  <li>
                    <a href="#" onClick={()=>setActiveTab({id:'Spotlight',name:lang.tabs.spotlight,langauge_name:"spotlight"})} className={`${activeTab.id=="Spotlight"?"active":""}`}>
                      {lang.tabs.spotlight}
                    </a>
                  </li>
                  <li>
                    <a href="#" onClick={()=>setActiveTab({id:'All user', name:lang.tabs.all_user, langauge_name:"all_user"})} className={`${activeTab.id=="All user"?"active":""}`}>
                      {lang.tabs.all_user}
                    </a>
                  </li>
                </ul>
              </Box> */}

              <Box className="card-body card__body py-md-4">
                {all_users && all_users.length > 0 ? (
                  <>
                    <Box className="user-listing d-flex pt-0">
                      {all_users.filter((x) => x.id !== user.id && x.gender===user_alt_gender)
                        .map((data, i) => {
                          return (
                            <Box className={`user-card ${ (onlineUsers.includes(data.id))?"online":"offline"}`} key={i}>
                              <Link
                                to={`/profile/${data?.id}`}
                                className="d-block h-100 span-child-100"
                              >
                                <Image
                                  className="w-100"
                                  src={userAvatar(data)}
                                  style={{
                                    height: "200px",
                                    maxHeight: "initial",
                                  }}
                                />
                                <Box className="user-footer">
                                  <p className="text-capitalize">{data?.name}</p>
                                  <span className={`badge ${data.gender === "male" ?"male":"female"}`}>
                                    <i
                                      className={`ri-${
                                        data.gender === "male" ? "men" : "women"
                                      }-line`}
                                    ></i>
                                    &nbsp;
                                    {data.birth_date
                                      ? getAge(data.birth_date)
                                      : "-"}
                                  </span>
                                </Box>
                              </Link>
                            </Box>
                          );
                        })}
                    </Box>
                  </>
                ) : 
                !loader ? (
                  <Heading className="py-5 text-center text-black mb-0">
                    {lang.no_users}
                  </Heading>
                ) : null}
              </Box>
            </Box>
          </div>
        </Box>
      </main>
    </>
  );
};

export default Home;
