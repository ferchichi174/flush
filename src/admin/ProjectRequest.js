import React from "react";
import { EyeFill, Trash2Fill } from "react-bootstrap-icons";
import { TableSearchBar } from "../components/Components";
import { Box, FlexBox, Label } from "../components/Style";
import DataTable from "../components/Ui/DataTable";
import Select from "react-select"
const ProjectRequest = () => {
  const ActionButtons = () => {
    return (
      <FlexBox>
        <button title="View" className="p-0 bg-transparent me-2 border-0">
          <EyeFill size={17} color="#1A75BC" />
        </button>
        <button title="Delete" className="p-0 bg-transparent border-0">
          <Trash2Fill size={17} color="#EB5757" />
        </button>
      </FlexBox>
    );
  };
  const qualification = [
    { value: "10", label: "10" },
    { value: "25", label: "25" },
    { value: "50", label: "50" },
    { value: "100", label: "100" },
  ]
  const column = [
    {
      title: "Sr. #",
      field: "id",
    },
    {
      title: "Business needs",
      field: "business_need",
    },
    {
      title: "Service required",
      field: "service",
    },
    {
      title: "Skills required",
      field: "skills",
    },
    {
      title: "Email address",
      field: "email",
    },
    {
      title: "Phone number",
      field: "number",
    },
    {
      title: "Action",
      field: "action",
    },
  ];
  const rows = [
    {
      id: "1",
      business_need: "Validating product idea, +1",
      service: "UX/UI Design, +1",
      skills: "IOS, Java, +3",
      email: "abc@gmail.com",
      number: "(000) 000 00000",
      action: <ActionButtons />,
    },
    {
      id: "2",
      business_need: "Validating product idea, +1",
      service: "UX/UI Design, +1",
      skills: "IOS, Java, +3",
      email: "abc@gmail.com",
      number: "(000) 000 00000",
      action: <ActionButtons />,
    },
    {
      id: "3",
      business_need: "Validating product idea, +1",
      service: "UX/UI Design, +1",
      skills: "IOS, Java, +3",
      email: "abc@gmail.com",
      number: "(000) 000 00000",
      action: <ActionButtons />,
    },
    {
      id: "4",
      business_need: "Validating product idea, +1",
      service: "UX/UI Design, +1",
      skills: "IOS, Java, +3",
      email: "abc@gmail.com",
      number: "(000) 000 00000",
      action: <ActionButtons />,
    },
    {
      id: "5",
      business_need: "Validating product idea, +1",
      service: "UX/UI Design, +1",
      skills: "IOS, Java, +3",
      email: "abc@gmail.com",
      number: "(000) 000 00000",
      action: <ActionButtons />,
    },
  ];
  return (
    <Box className="mt-4">
      <Box className="mt-4 pt-3">
        <FlexBox className="justify-content-between">
          <Box style={{ width: "233px" }} className="me-2">
            <TableSearchBar />
          </Box>
          <FlexBox className="mt-2">
            <Label className="me-2" size="13px" weight="400" style={{marginBottom:"20px"}}>Entries</Label>
            <Box style={{ width: "100px" }}>
              <Select options={qualification} />
            </Box>
          </FlexBox>
        </FlexBox>
        <DataTable rows={rows} column={column} />
      </Box>
    </Box>
  );
};

export default ProjectRequest;
