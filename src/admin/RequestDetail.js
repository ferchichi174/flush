import React from "react";
import { HeadingWithBorder } from "../components/Components";
import { Box, FlexBox, Heading, P } from "../components/Style";
import styled from "styled-components";
import { Row , Col } from "react-bootstrap";
const ListData = () => {
  return (
    <ol className="px-3 my-5">
      {[...Array(3)].map((x, i) => (
        <li className={i !== 3 ? "mb-3" : "mb-0"} key={i}>
          <P className="mb-0" size="12px" color="#2E2E2E">
            Lorem ipsum dolor sit.
          </P>
        </li>
      ))}
    </ol>
  );
};
const UserInfo = ({heading,text}) => {
  return (
    <>
      <Heading size={12} className="mb-0" weight={700}>{heading}</Heading>
      <P size="12px" className="mb-0" weight={400}>{text}</P>
    </>
  );
};
const RequestDetail = () => {
  return (
    <Box className="pe-3 mt-4">
      <HeadingWithBorder text="Business needs" />
      <ListData />
      <HeadingWithBorder text="Need help with" />
      <ListData />
      <HeadingWithBorder text="Skills needed" />
      <ListData />
      <HeadingWithBorder text="About project" />
      <ListData />
      <HeadingWithBorder text="About client" />
      <DetailContainer>
        <Box className="pt-1">
            <Row>
                <Col md={6} lg={4} className="mt-4">
                    <UserInfo heading="First name" text="John" />
                </Col>
                <Col md={6} lg={4} className="mt-4">
                    <UserInfo heading="Last name" text="Doe" />
                </Col>
                <Col md={6} lg={4} className="mt-4">
                    <UserInfo heading="Role" text="Lorem ipsum dolor sit" />
                </Col>
                <Col md={6} lg={4} className="mt-4">
                    <UserInfo heading="Company" text="Lorem ipsum dolor sit" />
                </Col>
                <Col md={6} lg={4} className="mt-4">
                    <UserInfo heading="Phone number" text="000000000000" />
                </Col>
                <Col md={6} lg={4} className="mt-4">
                    <UserInfo heading="Email address" text="abc@email.com" />
                </Col>
            </Row>
        </Box>
      </DetailContainer>
    </Box>
  );
};
const DetailContainer =  styled.div`
   max-width: 600px;
`
export default RequestDetail;
