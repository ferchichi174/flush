import React from "react";
import { Form } from "react-bootstrap";
import { Trash2Fill } from "react-bootstrap-icons";
import styled from "styled-components";
import { TableSearchBar } from "../components/Components";
import { Box, FlexBox, Input, Label } from "../components/Style";
import DataTable from "../components/Ui/DataTable";
import { Button } from "../pages/Careers";
const Category = () => {
  const ActionButtons = () => {
    return (
      <FlexBox>
        <button title="Delete" className="p-0 bg-transparent me-2 border-0">
          <Trash2Fill size={17} color="#EB5757" />
        </button>
        <Label size="12px" weight="400" className="d-flex align-items-center">
          <Form.Check
            as="input"
            className="switch-input-custom me-1"
            type="switch"
          />
          Inactive
        </Label>
      </FlexBox>
    );
  };
  const column = [
    {
      title: "Sr. #",
      field: "id",
    },
    {
      title: "Cateogry name",
      field: "category_name",
    },
    {
      title: "Action",
      field: "action",
    },
  ];
  const rows = [
    {
      id: "1",
      category_name: "Lorem ipsum dolor",
      action: <ActionButtons />,
    },
    {
      id: "2",
      category_name: "Lorem ipsum dolor",
      action: <ActionButtons />,
    },
  ];
  return (
    <MdContainer className="mt-4">
      <Box>
        <Label size="14px" weight={400} color="#2E2E2E">
          Add new category
        </Label>
        <Input className="mt-2" placeholder="Category name" type="text" />
        <Box className="text-end">
          <Button className="border-0 mt-0">Save</Button>
        </Box>
      </Box>
      <Box className="mt-4 pt-3">
        <Box style={{ width: "233px" }}>
          <TableSearchBar />
        </Box>
        <DataTable rows={rows} column={column} />
      </Box>
    </MdContainer>
  );
};
const MdContainer = styled.div`
  max-width: 665px;
`;
export default Category;
