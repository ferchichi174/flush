import React from 'react'
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import styled from 'styled-components';
import Select from "react-select"
import { Box, FlexBox, Input, Label } from '../components/Style';
import { Form } from 'react-bootstrap';
import { Paperclip, PencilFill, Plus } from 'react-bootstrap-icons';
import { Button } from '../pages/Careers';
const AddJob = () => {
    const qualification = [
        { value: "10", label: "10" },
        { value: "25", label: "25" },
        { value: "50", label: "50" },
        { value: "100", label: "100" },
    ]
    const modules = [
        ['bold', 'italic', 'underline'],      
        [{ 'list': 'ordered'}, { 'list': 'bullet' }]
    ];
  return (
    <BoxContainer className="mt-4">
        <Box className="mb-3">
            <Label className="mb-2">Job title</Label>
            <Input
                type="text"
                className="bg-transparent"
                placeholder="Enter job title"
            />
        </Box>
        <Box className="mb-3">
            <Label className="mb-2">Job Category</Label>
            <Select options={qualification} />
        </Box>
        <Box className="mb-3">
            <Label className="mb-2">Workspace</Label>
            <Select options={qualification} />
        </Box>
        <Box className="mb-3">
            <Label className="mb-2">Application deadline</Label>
            <Input
                type="date"
                className="bg-transparent"
                placeholder="Select application deadline"
            />
        </Box>
        <Box className="mb-3">
            <Label className="mb-2">Overview</Label>
            <Box className="position-relative">
                <Input style={{paddingLeft:"140px"}}
                    type="text"
                    className="bg-transparent"
                    placeholder="Overview text here"
                    readOnly={true} 
                />
                <FileSelector className="position-absolute">
                    <Label color="var(--theme-clr)">
                        <Form as="input" type="file" className="d-none"/>
                        <span><Paperclip size={18} /></span>&nbsp;Attach Icon<span>&nbsp;|</span>
                    </Label>
                </FileSelector>
            </Box>
            <Box className='mb-3'>
                <ExtraAddStyled className="d-flex align-items-center">
                    <Plus size={20}  />
                    <span>Add overview field</span>
                </ExtraAddStyled>
            </Box>
            <Box className="pt-4">
                <Box>
                    <FlexBox>
                        <Label className="mb-3">Responsibilities&nbsp;&nbsp;<PencilFill /></Label>
                    </FlexBox>
                    <ReactQuill theme="snow" modules={{toolbar:modules}}>
                        <div style={{ height: "150px" }} />
                    </ReactQuill>
                </Box>
            </Box>
            <Box className="pt-4">
                <Box>
                    <FlexBox>
                        <Label className="mb-3">Skills & Requirements&nbsp;&nbsp;<PencilFill /></Label>
                    </FlexBox>
                    <ReactQuill theme="snow" modules={{toolbar:modules}}>
                        <div style={{ height: "150px" }} />
                    </ReactQuill>
                </Box>
            </Box>
            <Box className='mt-4'>
                <ExtraAddStyled className="d-flex align-items-center">
                    <Plus size={20}  />
                    <span>Add new text field</span>
                </ExtraAddStyled>
            </Box>
        </Box>
        <Box className="text-end pt-4">
          <Button className="border-0 mt-0">Save</Button>
        </Box>
    </BoxContainer>
  )
}
const BoxContainer = styled.div`
  max-width:668px;
`;
const FileSelector = styled.div`
  top:12px;
  left:20px;
`;
const ExtraAddStyled = styled.button`
    border:0;
    background:transparent;
    color:var(--theme-clr);
    font-size:14px;
    line-height:1;
    font-weight:600;
    padding:0;
`;
export default AddJob;