import React from "react";
import { AttachFileButton, Box, FlexBox, Input, Label, P } from "../components/Style";
import { Paperclip } from "react-bootstrap-icons"; // , Facebook, Twitter, Linkedin
import styled from "styled-components";
import { Avatar } from "../components/AllImages"; // Logo, LogoPng 
import { Button } from "../pages/Careers";
import { Form } from "react-bootstrap";


const Profile = () => {
  return (
    <MdContainer>
      <Box className="mt-4">
        <P className="mb-3" color="#2E2E2E">
            Profile image
        </P>
        <FlexBox>
            <Box className="me-3">
                <img alt='avatar' src={Avatar} height="100px" width="100px" className="rounded-circle" />
            </Box>
            <AttachFileButton>
                <Paperclip size={20} />
                &nbsp;<span className="attach-head">Attach image</span>
                &nbsp;<span>|&nbsp;profile.jpg</span>
                <Form as="input" type="file" className="d-none" />
            </AttachFileButton>
        </FlexBox>
        <Box className="mt-5">
            <Label  size="14px" weight={400} color="#2E2E2E">
                Admin name
            </Label>
            <Input className="mt-2" value="John Doe" type="text" />
        </Box>
        <Box className="mt-3">
            <Label  size="14px" weight={400} color="#2E2E2E">
                Admin email
            </Label>
            <Input className="mt-2" value="admin@email.com" type="email" />
        </Box>
        <Box className="mt-3">
            <Label  size="14px" weight={400} color="#2E2E2E">
                Admin Password
            </Label>
            <Input className="mt-2" placeholder="*********" type="password" />
        </Box>
        <Box className="text-end">
            <Button className="border-0 mt-4">
                Update profile
            </Button>
        </Box>
      </Box>
    </MdContainer>
  );
};
const MdContainer = styled.div`
  max-width: 600px;
`;


export default Profile;
