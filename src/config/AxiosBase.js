import axios from "axios";
import { BASE_URL } from "../constant/Keys";

export default axios.create({
  baseURL:`${BASE_URL}`,
  responseType: "json",
});
