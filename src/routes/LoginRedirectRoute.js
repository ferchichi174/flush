import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router";

export default function LoginRedirectRoute({ component: Component, ...rest }) {
  const { token } = useSelector((state) => state.auth);
  return (
    <Route
      {...rest}
      render={(props) =>
        !token ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/home",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
}
