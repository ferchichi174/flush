import { combineReducers } from "redux";
import { authReducer } from "./authReducers";
import { AllClientReducer } from "./ClientReducer";
export default combineReducers({
  auth: authReducer,
  clients:AllClientReducer
});